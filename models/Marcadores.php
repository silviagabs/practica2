<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $prospecto
 * @property string $tipo
 * @property string $enlace
 */
class Marcadores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marcadores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'descripcion', 'prospecto', 'tipo', 'enlace'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'prospecto' => 'Descripción Larga',
            'tipo' => 'Tipo',
            'enlace' => 'Enlace',
        ];
    }

    /**
     * @inheritdoc
     * @return MarcadoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MarcadoresQuery(get_called_class());
    }
}
