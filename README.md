<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Basic Project Template</h1>
    <br>
</p>



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install via Composer

Ejecuta esto:

~~~
composer create-project silviagabs/practica2 carpeta
~~~

donde pone carpeta, debes poner el nombre de la carpeta donde se instalará este proyecto


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=p2',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

La base de datos con datos está en la carpeta data. 
La base de datos se llama p2. Crea una y este script creará la tabla correspondiente



