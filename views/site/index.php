<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marcadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marcadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

   <div class="celdas col-lg-4">
    ID
</div>
    <div class="celdas col-lg-4">
    ENLACE
</div>
    <div class="celdas col-lg-4">
   DESCRIPCION
</div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_registro',
        'summary'=>false,
    ]); ?>
</div>