<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

?>

<div class="celdas col-lg-4">
    <?= $model->id ?>
</div>
<div class="celdas col-lg-4">
    <a href="<?= $model->enlace ?> ">
        <?= $model->nombre ?>
</a>
</div>
<div class="celdas col-lg-4">
    <?php
        Modal::begin([
            'header'=>'<h2>DESCRIPCIÓN</h2>',
            'toggleButton'=>[
                'label'=>$model->descripcion,
                'class'=>'btn',
            ],
        ]);
    echo $model->prospecto;
    ?>
     <div class="row">
        
        <div class="col-lg-offset-8 col-lg-2">
            <a href="<?=$model->enlace?>" class="btn-info btn" target="_blank">Visitar</a>
        </div>
        <div class="col-lg-2">
            <?= Html::a("Cerrar",['site/index'],['class'=>'btn btn-info'])?>
        </div>
    </div>
    <?php
    Modal::end();
    ?>
</div>