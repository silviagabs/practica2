<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */

$this->title = 'Update Marcadores: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Marcadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="marcadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
