<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */


$this->params['breadcrumbs'][] = ['label' => 'Marcadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marcadores-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
