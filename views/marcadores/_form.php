<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marcadores-form">

    <?php $form = ActiveForm::begin(); ?>
   

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, ]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prospecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo')->dropDownList(['publico' => 'publico', 'privado' => 'privado']) ?>

     <?= $form->field($model, 'enlace')->textInput(['maxlength' => true]) ?> 

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-lg-offset-11']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
