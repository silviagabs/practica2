<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marcadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marcadores-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [

                'attribute' => 'nombre',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->nombre, ['marcadores/view', 'id' => $model->id]);
                }
                    ],
                    'descripcion',
                //['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>

            <p>
                <?= Html::a('Eliminar', ['delete'], ['class' => 'btn btn-success col-lg-offset-11']) ?>
    </p>
</div>
